\documentclass[11pt]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}  
\usepackage[T1]{fontenc}
\usepackage[left=2.7cm,right=2.7cm,top=3cm,bottom=3cm]{geometry}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{bbm}
\usepackage{hyperref}
\usepackage{multirow}

\graphicspath{{figures/}}

\title{
	\noindent\rule{\linewidth}{0.4pt}
	\huge Computational statistics --- TP 4 \\
	\Large \textit{Improve the Metropolis-Hastings algorithm}
	\noindent\rule{\linewidth}{1pt}
}

\author{Yoann Coudert-\,-Osmont}

\DeclareMathOperator{\com}{com}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\newcommand{\trans}{\mathsf{T}}
\newcommand{\syst}[2]{\left\{ \begin{array}{#1} #2 \end{array} \right.}
\newcommand{\diag}{\text{\textbf{diag}}~}
\newcommand{\pmat}[1]{\begin{pmatrix} #1 \end{pmatrix}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Pp}{\mathbb{P}}
\newcommand{\ind}[1]{\mathbbm{1}_{#1}}

\begin{document}
	
	\maketitle
	
	\section*{Exercise 3: Bayesian analysis of a one-way random effects model}
	
	\paragraph{1.}
	On utilise d'abord la définition de la probabilité conditionnelle :
	$$ \Pp \left( X, \mu, \sigma^2, \tau^2 \mid Y \right) = \dfrac{\Pp \left( Y, X, \mu, \sigma^2, \tau^2 \right)}{\Pp(Y)} $$
	On remarque que $\Pp(Y)$ ne dépend pas des autres paramètres. On va donc écrire la probabilité à posteriori à cette constante de normalisation près. De plus on peut décomposer en chaîne la grande probabilité jointe pour obtenir :
	$$ \Pp \left( X, \mu, \sigma^2, \tau^2 \mid Y \right) \propto \Pp \left( Y \mid X, \tau^2 \right) \Pp \left( X \mid \mu, \sigma^2 \right) \pi \left( \mu, \sigma^2, \tau^2 \right) $$
	On connaît les deux dernière probabilités mais il nous faut expliciter la première :
	$$ \Pp \left( Y=y \mid X=x, \tau^2 \right) = \Pp(\epsilon = y-x \mid \tau^2) $$
	Sous cette forme on peut alors donner l'expression finale de notre probabilité à posteriori :
	\begin{center}
		\fbox{$ \begin{array}{lll}
			\Pp \left( X, \mu, \sigma^2, \tau^2 \mid Y \right) & \propto & \displaystyle \tau^{-k} \prod_{i = 1}^N \prod_{j = 1}^{k_i} \exp \left( - \dfrac{(x_i - y_{ij})^2}{2 \tau^2} \right) \, \cdot \, \sigma^{-N} \prod_{i = 1}^N \exp \left( - \dfrac{(x_i - \mu)^2}{2 \sigma^2} \right) \\[5mm]
			& & \displaystyle \cdot \, \sigma^{-2(1 + \alpha)} \exp \left( - \dfrac{\beta}{\sigma^2} \right) \tau^{-2(1 + \gamma)} \exp \left( - \dfrac{\beta}{\tau^2} \right)
			\end{array} $}
	\end{center}

	\paragraph{2.}
	On a besoin de calculer les probabilités conditionnelles pour chaque paramètre. A chaque fois on ne conserve que les facteurs qui contiennent le paramètre considéré dans la loi à posteriori. Commençons par $X$ :
	$$ \Pp \left( X \mid Y, \mu, \sigma^2, \tau^2 \right) \propto \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \left[ \dfrac{(x_i - \mu)^2}{\sigma^2} + \sum_{j = 1}^{k_i} \dfrac{(x_i - y_{ij})^2}{\tau^2} \right] \right) = \prod_{i = 1}^N \exp \left( - \dfrac{1}{2} P_i(x_i) \right) $$
	Où les $P_i$ sont des polynômes d'ordre 2. Ainsi les $x_i$ sont indépendants et suivent chacun une loi normale d'espérance $m_i$ et de variance $s_i^2$ où :
	$$ P_i(x_i) = \dfrac{1}{s_i^2} x_i^2 - \dfrac{2 m_i}{s_i^2} x_i + c^{te} $$
	On développe note expression de $P_i$ :
	$$ P_i(x_i) = \left( \sigma^{-2} + k_i \tau^{-2} \right) x_i^2 - 2 \left( \dfrac{\mu}{\sigma^2} + \sum_{j = 1}^{k_i} \dfrac{y_{ij}}{\tau^2} \right) x_i + c^{te} $$
	Cela nous permet d'identifier :
	\begin{center}
		\fbox{$ \displaystyle x_i \mid Y, \mu, \sigma^2, \tau^2 \, \sim \, \mathcal{N} \left( m_i, s_i^2 \right) \qquad \text{avec} \qquad s_i^2 = \left( \sigma^{-2} + k_i \tau^{-2} \right)^{-1} \quad \text{et} \quad m_i = s_i^2 \left( \dfrac{\mu}{\sigma^2} + \sum_{j = 1}^{k_i} \dfrac{y_{ij}}{\tau^2} \right) $}
	\end{center}

	\paragraph{}
	Désormais on s'intéresse à $\mu$. On utilise le même procédé :
	$$ \Pp \left( \mu \mid X, \sigma^2 \right) \propto \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \dfrac{(x_i - \mu)^2}{\sigma^2} \right) \propto \exp \left( - \dfrac{1}{2} \left[ \dfrac{N}{\sigma^2} \mu^2 - \dfrac{2}{\sigma^2} \sum_{i = 1}^N x_i \mu \right] \right) $$
	On peut identifier cette probabilité avec une loi normale et on obtient :
	\begin{center}
		\fbox{$ \displaystyle \mu \mid X, \sigma^2 \, \sim \, \mathcal{N} \left( \dfrac{1}{N} \sum_{i = 1}^N x_i, \, \dfrac{\sigma^2}{N} \right)$}
	\end{center}

	\paragraph{}
	Maintenant on s'intéresse aux deux variances $\sigma^2$ et $\tau^2$. Voici alors les deux probabilités conditionnelles de ces paramètres à une constante de normalisation près :
	$$ \Pp \left( \sigma^2 \mid X, \mu \right) \propto \dfrac{1}{\sigma^N} \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \dfrac{(x_i - \mu)^2}{\sigma^2} \right) \cdot \dfrac{1}{\sigma^{2(1 + \alpha)}} \exp \left( \dfrac{\beta}{\sigma^2} \right) $$
	$$ \Pp \left( \tau^2 \mid Y, X \right) \propto \dfrac{1}{\tau^k} \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \sum_{j = 1}^{k_i} \dfrac{(x_i - y_{ij})^2}{\tau^2} \right) \cdot \dfrac{1}{\tau^{2(1 + \gamma)}} \exp \left( \dfrac{\beta}{\tau^2} \right) $$
	On reconnaît deux loi inverse gamma. On identifie les paramètres et on obtient~:
	\begin{center}
	\fbox{$ \displaystyle \sigma^2 \mid X, \mu \, \sim \, inv \Gamma \left( \alpha + \dfrac{N}{2}, \; \beta + \dfrac{1}{2} \sum_{i = 1}^N \left( x_i - \mu \right)^2 \right) \qquad \tau^2 \mid Y, X \, \sim \, inv \Gamma \left( \gamma + \dfrac{k}{2}, \; \beta + \dfrac{1}{2} \sum_{i = 1}^N \sum_{j = 1}^{k_i} \left( x_i - y_{ij} \right)^2 \right) $}
	\end{center}

	\paragraph{3.}
	C'est fois ci on s'intéresse à la loi joint $(X, \mu)$ conditionnellement aux autres paramètres. On note dans un premier temps la relation suivante :
	$$ \Pp \left( X, \mu \mid Y, \sigma^2, \tau^2 \right) = \Pp \left( X \mid Y, \sigma^2, \tau^2 \right) \Pp \left( \mu \mid X, \sigma^2 \right) $$
	Le second facteur a été calculé à la question précédente. Il nous reste plus qu'à calculer le premier facteur. On a :
	$$ \begin{array}{lll}
		\Pp \left( X \mid Y, \sigma^2, \tau^2 \right) & \propto & \displaystyle \int_\R \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \dfrac{(x_i - \mu)^2}{\sigma^2} \right) \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \sum_{j = 1}^{k_i} \dfrac{(x_i - y_{ij})^2}{\tau^2} \right) d\mu \\[5mm]
		& \propto & \displaystyle \sqrt{\dfrac{2 \pi \sigma^2}{N}} \exp \left( - \dfrac{1}{2} \sum_{i = 1}^N \sum_{j = 1}^{k_i} \dfrac{(x_i - y_{ij})^2}{\tau^2} \right) \\[5mm]
		& \propto & \displaystyle \prod_{i = 1}^N \exp \left( - \dfrac{1}{2} \sum_{j = 1}^{k_i} \dfrac{(x_i - y_{ij})^2}{\tau^2} \right)
	\end{array} $$
	A nouveau, on reconnaît que les $x_i$ sont indépendants et suivent des lois normales :
	\begin{center}
		\fbox{$ \displaystyle x_i \mid Y, \tau^2 \, \sim \, \mathcal{N} \left( \dfrac{1}{k_i} \sum_{j = 1}^{k_i} y_{ij}, \; \dfrac{\tau^2}{k_i} \right) $}
	\end{center}
	Pour tirer le bloc $(X, \mu)$, on tire alors les $x_i$ selon ces loi puis on tire $\mu$ selon la loi trouvé à la question précédente.
	
	\paragraph{4.}
	Les deux algorithmes font plutôt bien le travail et on des auto-corrélations assez similaires bien qu'on aurait pu s'attendre à ce que le Block-Gibbs sampler ait moins d'auto-corrélation. En effet, moins on considère de blocs plus on se rapproche de la loi jointe complète de tous les paramètres et donc plus on tend vers une auto-corrélation nulle.
	
\end{document}