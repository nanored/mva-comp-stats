# TP2 Statistiques computationnelles

Cet archive contient un notebook [tp2_ycoudert.ipynb](tp2_ycoudert.ipynb) avec les réponses aux différentes questions de l'exercice 1 et 2. Si besoin le code python se trouve aussi dans les scripts [exo1.py](exo1.py) et [exo2.py](exo2.py) mais ces derniers ne contienent aucune rédaction.