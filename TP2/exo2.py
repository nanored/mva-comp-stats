import numpy as np
import pylab as pl
from matplotlib.patches import Ellipse
import exo1

import matplotlib.animation as anim
import matplotlib as mpl
mpl.rcParams['figure.subplot.left'] = 0
mpl.rcParams['figure.subplot.right'] = 1
mpl.rcParams['figure.subplot.bottom'] = 0
mpl.rcParams['figure.subplot.top'] = 1

####### PARAMETERS #######
m = 4
n = 2000
w, h = 80, 40
nSteps = 50
animation = False
##########################

### True parameters generation ###

def random_mixture(m, w, h):
	# Generate alpha
	alpha = np.random.rand(m) + np.ones(m)*0.1
	alpha /= sum(alpha)
	# Generate and plot mu
	sig = 0.2 * np.sqrt(w*h/m)
	mu = np.random.rand(m, 2) * np.array([w-2*sig, h-2*sig]) + np.array([sig, sig])
	# Generate and plot sigma
	sigma = []
	for j in range(m):
		v1, v2, angle = np.random.rand(3) * [0.7*sig, 0.4*sig, 2*np.pi] + [0.3*sig, 0.2*sig, 0]
		c, s = np.cos(angle), np.sin(angle)
		D = np.diag([v1*v1, v2*v2])
		R = np.matrix([[c, -s], [s, c]])
		S = R * D * R.transpose()
		sigma.append(S)
	sigma = np.array(sigma)
	return alpha, mu, sigma

ims = [[]]

def plot_gaussians(mu, sigma, alpha, ax, color='r'):
	m = len(mu)
	ims[-1].append(pl.scatter(mu[:,0], mu[:,1], color=color, s=2e3/n))
	for j in range(m):
		w, v = np.linalg.eigh(sigma[j])
		for mul in [1.0, 1.8, 2.6]:
			angle = np.math.atan2(v[1, 0], v[0, 0])
			el = Ellipse(mu[j], mul*w[0]**0.5, mul*w[1]**0.5, 180/np.pi*angle, facecolor='none',
							edgecolor=color, linestyle='--', linewidth=0.35+0.7*m*alpha[j])
			ims[-1].append(ax.add_patch(el))

def plot_observations(X):
	ims[-1].append(pl.scatter(X[:,0], X[:,1], color='b', s=5e2/len(X)))
	x0, x1 = min(X[:,0]), max(X[:,0])
	y0, y1 = min(X[:,1]), max(X[:,1])
	pl.xlim(x0, x1)
	pl.ylim(y0, y1)

### Sampling ###

def sample(alpha, mu, sigma):
	Salpha = exo1.cumulative(alpha)
	Z = exo1.draw(Salpha, n) - 1
	X = [np.random.multivariate_normal(mu[z], sigma[z]) for z in Z]
	X = np.array(X)
	return X

### Figure ###

fig, ax = pl.subplots()
alpha0, mu0, sigma0 = random_mixture(m, w, h)
X = sample(alpha0, mu0, sigma0)

if not animation:
	plot_gaussians(mu0, sigma0, alpha0, ax)
	plot_observations(X)

### EM Algorithm ###

def EM(X, m, nSteps, verbose=True):
	if animation: plot_observations(X)
	n, d = X.shape
	# Mean of X
	mX = X.mean(0)
	# Covariance of X
	cX = X - mX
	sX = cX.T @ cX / len(X)
	# Initial parameters
	alpha = np.ones(m) / m
	mu = np.array([np.random.multivariate_normal(mX, sX) for _ in range(m)])
	sigma = np.array([sX / m**2] * m)
	# Function to center X with respect to mu
	centered = lambda: X.reshape(n, 1, d, 1) - mu.reshape(1, m, d, 1)
	ll = -float('inf')
	for t in range(nSteps+1):
		# E step
		invSigma = np.linalg.pinv(sigma)
		detSigma = np.linalg.det(sigma)
		if t == 0: Xc = centered()
		cov = Xc.transpose((0, 1, 3, 2)) @ invSigma.reshape(1, m, d, d) @ Xc
		tau = np.exp(-0.5 * cov.reshape(n, m)) * alpha / (detSigma * (2*np.pi)**d + 1e-10) ** 0.5
		L = tau.sum(1)
		nll = np.log(L).sum()
		if nll - ll < 0.1: break
		ll = nll
		if verbose: print("log-likelihood:", ll)
		if animation:
			ims.append([ims[-1][0]])
			plot_gaussians(mu, sigma, alpha, ax)
		if t == nSteps: break
		tau /= L.reshape(n, 1)

		# M step
		alpha = tau.mean(0) + 1e-10
		alpha /= alpha.sum()
		mu = (tau.reshape(n, m, 1) * X.reshape(n, 1, d)).mean(0) / alpha.reshape(m, 1)
		Xc = centered()
		sigma = (tau.reshape(n, m, 1, 1) * (Xc @ Xc.transpose((0, 1, 3, 2)))).mean(0) / alpha.reshape(m, 1 ,1)
	return alpha, mu, sigma

if animation:
	while len(ims) < 24:
		ims = [[]]
		ax.cla()
		alpha, mu, sigma = EM(X, m, nSteps)
		ims.append([ims[-1][0]])
		plot_gaussians(mu, sigma, alpha, ax)

	ani = anim.ArtistAnimation(fig, ims, interval=333, repeat=True)
	ani.save('res.gif', writer='imagemagick')

else:
	alpha, mu, sigma = EM(X, m, nSteps)
	plot_gaussians(mu, sigma, alpha, ax, 'g')

pl.show()
if animation: exit(0)

### Distance ###

def sqrtm(A):
	v, w = np.linalg.eigh(A)
	return w @ np.diag(np.sqrt(v)) @ w.T

def wasserstain_gaussian(mu0, sigma0, mu1, sigma1):
	dm = mu0 - mu1
	ndm = dm.T @ dm
	sqrtSig = sqrtm(sigma0)
	ds = np.trace(sigma0 + sigma1 - 2 * sqrtm(sqrtSig @ sigma1 @ sqrtSig))
	return (ndm + ds) ** 0.5

def mine(z, e, axis):
	m = z.min(axis)
	shape = list(z.shape)
	shape[axis] = 1
	s = np.exp((m.reshape(shape)-z) / e).sum(axis)
	return m - e * np.log(s)

def sinkhorn(a, b, C, e, l=100000):
	n = len(a)
	l //= n*len(b)
	ela, elb = e*np.log(np.maximum(a, 1e-20)), e*np.log(np.maximum(b, 1e-20))
	f, g = np.zeros(n), np.ones(m)
	stop = 1e-3 / n
	for step in range(l):
		x = C - f.reshape(n, 1) - g
		if abs(np.exp(-x/e).sum(1) - a).max() < stop: break
		f += mine(x, e, 1) + ela
		x = C - f.reshape(n, 1) - g
		g += mine(x, e, 0) + elb
	return np.exp((f.reshape(n, 1) + g - C) / e)

def mixture_dist_Wasserstein(alpha0, mu0, sigma0, alpha1, mu1, sigma1):
	n, m = len(mu0), len(mu1)
	ds = [[wasserstain_gaussian(mu0[i], sigma0[i], mu1[j], sigma1[j]) for j in range(m)] for i in range(n)]
	ds = np.array(ds) ** 2
	e = 0.01 * ds.mean()
	t = sinkhorn(alpha0, alpha1, ds, e)
	return (t*ds).sum() ** 0.5

def mixture_dist_L2(alpha0, mu0, sigma0, alpha1, mu1, sigma1):
	n, m = len(mu0), len(mu1)
	ds = np.array([[np.linalg.norm(sqrtm(sigma0[i]) - sqrtm(sigma1[j]))**2 for j in range(m)] for i in range(n)])
	ds += [[np.linalg.norm(mu0[i] - mu1[j])**2 for j in range(m)] for i in range(n)]
	ds += 0.5*np.amax(ds) * (alpha0.reshape(n, 1) - alpha1) ** 2
	e = 0.01 * ds.mean()
	t = sinkhorn(np.ones(n)/n, np.ones(m)/m, ds, e)
	return (t*ds).sum() ** 0.5

dis = mixture_dist_Wasserstein(alpha0, mu0, sigma0, alpha, mu, sigma)
dis2 = mixture_dist_L2(alpha0, mu0, sigma0, alpha, mu, sigma)
x0, x1 = min(X[:,0]), max(X[:,0])
y0, y1 = min(X[:,1]), max(X[:,1])
diag = ((x1-x0)**2+(y1-y0)**2)**0.5
print(f"Wasserstein distance: {dis} (normalized: {dis/diag})")
print(f"L2 distance: {dis2} (normalized: {dis2/diag})")

### Birth/Death loading ###

f = open("data.csv")
f.readline()
f.readline()
X = []
for l in f.readlines():
	l = l.replace('"', '').split(',')[1:]
	if '' in l: continue
	X.append(list(map(float, l)))
X = np.array(X)

### BIC ###

def df(m):
    return m*5 + (m-1)

def logL(X, alpha, mu, sigma):
    n, d = X.shape
    m = len(alpha)
    invSigma = np.linalg.pinv(sigma)
    detSigma = np.linalg.det(sigma)
    Xc = X.reshape(n, 1, d, 1) - mu.reshape(1, m, d, 1)
    cov = Xc.transpose((0, 1, 3, 2)) @ invSigma.reshape(1, m, d, d) @ Xc
    tau = np.exp(-0.5 * cov.reshape(n, m)) * alpha / (detSigma * (2*np.pi)**d + 1e-10) ** 0.5
    L = np.log(tau.sum(1)).sum()
    return L

def BIC(X, nSteps, maxM, nTry):
    n = len(X)
    maxM = min(n, maxM)
    bestS, bestM, bestT = float('inf'), 0, None
    for m in range(1, maxM+1):
        for _ in range(nTry):
            theta = EM(X, m, nSteps, False)
            alpha, mu, sigma = theta
            L = logL(X, alpha, mu, sigma)
            score = -L + 0.5*df(m)*np.log(n)
            if score < bestS:
                bestS, bestM, bestT = score, m, theta
    return bestM, bestT

### Plot ###

m_opt, theta = BIC(X, nSteps, 10, 3)
alpha, mu, sigma = theta
fig, ax = pl.subplots()
plot_observations(X)
plot_gaussians(mu, sigma, alpha, ax)

x0, x1 = min(X[:,0]), max(X[:,0])
y0, y1 = min(X[:,1]), max(X[:,1])
x = np.arange(x0, x1, 0.005*(x1-x0))
y = np.arange(y0, y1, 0.01*(y1-y0))
nx, ny = len(x), len(y)
x, y = np.meshgrid(x, y)
invSigma = np.linalg.inv(sigma)
detSigma = np.linalg.det(sigma)
xc = x.reshape(ny, nx, 1, 1, 1) - mu[:,0].reshape(m_opt, 1, 1)
yc = y.reshape(ny, nx, 1, 1, 1) - mu[:,1].reshape(m_opt, 1, 1)
Xc = np.concatenate((xc, yc), 3)
cov = Xc.transpose((0, 1, 2, 4, 3)) @ invSigma.reshape(1, 1, m_opt, 2, 2) @ Xc
cov = cov.reshape(ny, nx, m_opt)
z = np.exp(-0.5 * cov) * alpha / (detSigma * (2*np.pi)**2 + 1e-10) ** 0.5
z = z.sum(2)

# plot_observations(X)
cs = ax.contour(x, y, z)

pl.show()