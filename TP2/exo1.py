import numpy as np
import pylab as pl

def cumulative(p):
	"""
	Return the cumulative function of a probability distribution

	Parameters
	----------
	p : array of float
		The probability distribution
	"""
	S = [0]
	n = len(p)
	for pi in p:
		S.append(S[-1]+pi)
	return np.array(S+[1])

def draw(S, N):
	"""
	Draw and return a sequence of samples iid

	Parameters
	----------
	S : array of float
		The cumulative function of the distribution
	N : int
		Number of samples to be generated
	"""
	U = sorted(np.random.rand(N))
	X = []
	i = 1
	for u in U:
		while u >= S[i]:
			i += 1
		X.append(i)
	X = np.array(X)
	np.random.shuffle(X)
	return X

if __name__ == "__main__":
	####### PARAMETERS #######
	n = 10
	N = 1000
	##########################
	
	p = np.random.rand(n)
	p /= sum(p)
	S = cumulative(p)
	X = draw(S, N)
	pl.bar([1+i for i in range(n)], N*p, color='r', alpha=0.5, label='True distribution')
	pl.hist(X, [0.5+i for i in range(n+1)], rwidth=0.7, color='b', alpha=0.5, label='Sample histogram')
	pl.legend()
	pl.show()