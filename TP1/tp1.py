import numpy as np
import pylab as pl
import matplotlib.animation as anim

fig = pl.figure(figsize=(6, 6))

def inverseSeq():
	"""
	Generator of the infinite sequence of inversed integers
	"""
	n = 1
	while True:
		yield 1 / n
		n += 1

def drawUnitVector():
	"""
	Return a random 2D vector drawn uniformly on the unit sphere
	"""
	theta = np.random.uniform(0, 2*np.math.pi)
	return np.array((np.math.cos(theta), np.math.sin(theta)))

def generate_sample(n):
	"""
	Generate and return a list of observations `(x, y)` with:
	- `x` a 2D vector drawn uniformly in the 2D ball
	- `y` is the label of x in {-1, 1} depending on the side of the hyperplane `w` the point `x` is

	Where `w` is a hyperplane drawn uniformly on the 2D sphere

	Parameters
	----------
	n : int
		The number of observations generated
	"""
	w = drawUnitVector()
	print("Data generated with w =", w)
	sample = []
	for _ in range(n):
		r = np.random.rand()
		x = (r ** 0.5) * drawUnitVector()
		y = 1 if np.dot(w, x) > 0 else -1
		sample.append([x, y])
	return sample

def plot_sep(w):
	"""
	Plot the separation line defined by `w`

	Parameters
	----------
	w : array of float
		Represent the normal vector of the hyperplane to plot
	"""
	sep = (1.2 / (w[0]**2 + w[1]**2))**0.5 * np.array((-w[1], w[0]))
	return pl.plot([-sep[0], sep[0]], [-sep[1], sep[1]], color='black')[0]

def plot_result(Z, w):
	"""
	Plot observations and the hyperplane obtained in the 2D plane

	Parameters
	----------
	Z : list of observations
	w : array of float
		Represent the normal vector of the hyperplane to plot
	"""
	xs = {1:[], -1:[]}
	ys = {1:[], -1:[]}
	for x, y in Z:
		xs[y].append(x[0])
		ys[y].append(x[1])
	a = pl.scatter(xs[1], ys[1], color='b')
	b = pl.scatter(xs[-1], ys[-1], color='r')
	c = plot_sep(w)
	return a, b, c

def addNoise(Z, sigma):
	"""
	Add some gaussian noise to a list of observations

	Parameters
	----------
	Z : list of observations
	sigma : float
		The Standard deviation used in the gaussian
	"""
	for x, _ in Z:
		x += np.random.normal(np.array([0, 0]), sigma)

def score(Z, w):
	"""
	Return the score of the hyperplane `w` on the observations of `Z`. The score is the ratio between
	the number of correctly predicted observations and the total number of observations.

	Parameters
	----------
	Z : list of observations
	w : array of float
		Represent the normal vector of the hyperplane for which we want to know the score
	"""
	n = len(Z)
	correct = 0
	for x, y in Z:
		if y*np.dot(w, x) > 0: correct += 1
	return correct / n

def stochastic_gradient_descent(Z, w0, epsilon, K, nFrames=None):
	"""
	Compute and return a hyperplane `w` that separates labeled observations contained in `Z` using a
	stochastic gradient descent.

	Parameters
	----------
	Z : list of observations
	w0 : array of float
		Represent the hyperplane used to start the algorithm
	epsilon : float generator
		This generator is used to scale the updates of the `w`
	K : int
		Maximal number of steps of the algorithm
	nFrames : int or None, optional
		If different from None then it is the number of frames to show in a gif saved in the file `res.gif`
	"""
	w = w0.copy()
	if nFrames != None:
		ims = []
		a, b, c = plot_result(Z, w)
		ims.append([a, b, c, pl.text(1, 1, 'k = 0')])
		p = max(K // (nFrames - 1), 1)
	n = len(Z)
	for k in range(K):
		# Draw an observation
		x, y = Z[np.random.randint(n)]
		# Computation of the gradient
		grad = - 2 * (y - np.dot(w, x)) * x
		# Update of w
		e = next(epsilon)
		w -= e * grad
		# Projection on the unit sphere
		w /= np.linalg.norm(w)
		# Plot the hyperplane if needed
		if nFrames != None and (k+1) % p == 0:
			ims.append([a, b, plot_sep(w), pl.text(1, 1, f'k = {k}')])
	if nFrames != None:
		ani = anim.ArtistAnimation(fig, ims, interval=300, repeat=True)
		ani.save('res.gif', writer='imagemagick')
	return w

####### PARAMETERS #######
n = 100
sigma = 0.2
K = 500
nFrames = None      # If you want to create a gif of the gradient descent
##########################

print("==== RANDOM DATA ====")
Z = generate_sample(n)
addNoise(Z, sigma)
w_opt = stochastic_gradient_descent(Z, drawUnitVector(), inverseSeq(), K, nFrames)
print("Hyperplan learned w =", w_opt)
print("Obtained score (ratio of well predicted observations) is s =", score(Z, w_opt))
pl.clf()
plot_result(Z, w_opt)
pl.show()

### LAST QUESTION ###

####### PARAMETERS #######
K2 = 10000
##########################

print("\n==== Breast Cancer Wisconsin ====")
f = open("./breast-cancer-wisconsin.data", "r")
Z2 = []
for l in f.readlines():
	if '?' in l: continue
	l = l.split(',')
	x = (np.array(list(map(float, l[1:-1]))) - 5) / (9*5**2) ** 0.5
	y = int(l[-1]) - 3
	Z2.append([x, y])
w0 = (np.random.rand(9) - 0.5)
w_opt2 = stochastic_gradient_descent(Z2, w0, inverseSeq(), K2)
print("Hyperplan learned w =", w_opt2)
print("Obtained score (ratio of well predicted observations) is s =", score(Z2, w_opt2))