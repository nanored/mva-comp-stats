# TP1 Statistiques computationnelles

## Exercice 3

Cet archive contient un notebook [tp1.ipynb](tp1.ipynb) avec les réponses aux différentes questions de l'exercice 3. Si besoin le code python se trouve aussi dans le script [tp1.py](tp1.py) mais ce script ne contient aucune rédaction.
